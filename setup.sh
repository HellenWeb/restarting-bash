#!/bin/bash

cf='\033[0m' 
red='\033[1;31m'
rset='\033[0m'
grn='\033[1;32m'
ylo='\033[1;33m'
blue='\033[1;34m'
cyan='\033[1;36m'
pink='\033[1;35m'

# Setup script in /usr/local/bin
# To access from any directory
# || \\

export PATH=/usr/local/bin:$PATH
cp ./restarting.sh /usr/local/bin/restarting
chmod +x /usr/local/bin/restarting

# Message about success install

echo -e "\n     ---------[-]---------"
echo -e "           -  | |  -"
echo -e "     [*] Success setup [*]"
echo -e "              | |"
echo -e "     ---------------------\n"
echo -e " Simple bash script for easy restart"
echo -e " of many services and display status"
echo -e "   services on your linux system\n"
echo -e "       restarting --help\n\n"

