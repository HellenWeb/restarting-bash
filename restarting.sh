#!/bin/bash

# Colors for display

cf='\033[0m' 
red='\033[1;31m'
rset='\033[0m'
grn='\033[1;32m'
ylo='\033[1;33m'
blue='\033[1;34m'
cyan='\033[1;36m'
pink='\033[1;35m'

# Function for work with services

restart_service() {
    echo ""
    echo -n -e "[*] Enter your way (start, stop, restart): "; read chs
    if [[ $chs == "start" || $chs == "stop" || $chs == "restart" ]]; then
        echo ""
        while true; do
            echo -n -e "Enter util (Cancel: 1): "; read util
            if [[ $util == "1" ]]; then
                break
            fi
            array[${#array[@]}]=$util
        done
        echo ""
        for i in ${array[@]}; do
            echo -e "[*] $chs $i"; systemctl $chs $i 
        done
    else
        echo -n -e "\n[-] Incorrect value\n"
    fi
    echo -e "$cf"
    exit 0
}

# Function for show all files in directory

stat() {
    echo ""
    while true; do
        echo -n -e "Enter util (Cancel: 1): "; read util
        if [[ $util == "1" ]]; then
            break
        fi
        array[${#array[@]}]=$util
    done
    echo ""
    for i in ${array[@]}; do
        echo -n "[*]  "; systemctl status $i | grep "Active"
    done
    echo ""
    exit 0
}

# Function for help message

help() {

    # <| Help Message |> #
    
    echo -n -e "usage: restarting.sh [-h] [-v] [--status | -st] [-services | -s]
    options:
    --help, -h  show this help message and exit
    --status, -st Show status services
    --services, -s  Work with services
    --version, -v  Version your app\n"
    exit 0
}

# Work with flags and arguments

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      help
      exit 0
      ;;
    --status|-st)
      stat
      shift
      ;;
    --services|-s)
      restart_service
      shift
      ;;
    --version|-v)
      echo -n -e "Version 0.1.2 - Restarting-Bash\n"
      exit 0
      ;;
    *)
        echo -e "[-] Incorrect flags"
        help
        exit 0
        ;;
  esac
done

echo -e "[-] Incorrect flags"
help
exit 0
